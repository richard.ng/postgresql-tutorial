# PostgreSQL tutorial

Create .env file in root directory:
```
PG_USER=<POSTGRES_USER>
PG_HOST=<POSTGRES_HOST>
PG_DATABASE=<POSTGRES_DATABASE>
PG_PASSWORD=<POSTGRES_PASSWORD>
PG_PORT=<POSTGRES_PORT>
```
